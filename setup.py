# -*- Mode: python; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-

from setuptools import setup

setup_opts = dict(
    name='coti_lottery',
    author='Serge Emond',
    author_email='greyl@greyworld.net',
    packages=['coti_lottery'],
    py_modules=['coti_lottery'],
    include_package_data=True,
    install_requires=[
        'natsort',

        # 'randomSources',
        'randomdotorg',

        'Flask',
    ],
)

if __name__ == '__main__':
    setup(**setup_opts)
