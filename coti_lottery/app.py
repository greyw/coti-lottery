# -*- coding: utf-8 -*-
"""

:copyright: © 2016, Serge Émond
:license: not specified

"""

from __future__ import absolute_import

from flask import Flask

app = Flask(__name__)

from .views import bp
app.register_blueprint(bp, url_prefix='/')

