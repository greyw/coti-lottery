# -*- coding: utf-8 -*-
"""

:copyright: © 2016, Serge Émond
:license: not specified

"""

from __future__ import absolute_import

import re
import itertools
import randomdotorg


def messup_coti_members(members_with_n):
    """Takes a list of name + n participations, returns a randomized list.

    `members_with_n` is a list of tuples (n, name)

    Returns a list of names.
    """
    members = list(itertools.chain(*[
        itertools.repeat(i[1], i[0])
        for i in members_with_n
    ]))

    rdo = randomdotorg.RandomDotOrg("CotiOne")
    rdo.shuffle(members)

    return members


def clean_list(input):
    """Iterator returning usable (n, name) from user input.

    `input` is a string of the form "<n> <username>\n"
    Returns an iterator of tuples (n, name).
    """
    lines = re.split('[\r\n]+', input)
    pat = re.compile('\s*([0-9]+)\s+(.+)')
    for line in lines:
        m = pat.match(line)
        if m:
            yield (int(m.group(1)), m.group(2))
