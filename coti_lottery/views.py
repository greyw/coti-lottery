# -*- coding: utf-8 -*-
"""

:copyright: © 2016, Serge Émond
:license: not specified

"""

from __future__ import absolute_import

import unicodedata
from natsort import natsorted
from flask import Blueprint, render_template, request

from .shuffler import messup_coti_members, clean_list


bp = Blueprint('coti-lottery', __name__)


@bp.route('/', methods=['GET'])
def home():
    return render_template('form.html')


@bp.route('/', methods=['POST'])
def post_list():
    input = request.form.get('participations', None)
    if input:
        parts = natsorted(
            clean_list(input),
            key=lambda i: u"{nrm} {i[0]}".format(
                i=i, nrm=unicodedata.normalize('NFD', i[1])))
        total_parts = sum(i[0] for i in parts)
        shuffled = messup_coti_members(parts)
        return render_template(
            'form.html',
            participants=parts,
            total=total_parts,
            shuffled=shuffled
        )
    return render_template('form.html')
